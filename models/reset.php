<!--Quelle: https://stackoverflow.com/questions/4027769/running-mysql-sql-files-in-php -->

<?php

function parseScript($script) {

    $result = array();
    $delimiter = ';';
    while(strlen($script) && preg_match('/((DELIMITER)[ ]+([^\n\r])|[' . $delimiter . ']|$)/is', $script, $matches, PREG_OFFSET_CAPTURE)) {
        if (count($matches) > 2) {
            $delimiter = $matches[3][0];
            $script = substr($script, $matches[3][1] + 1);
        } else {
            if (strlen($statement = trim(substr($script, 0, $matches[0][1])))) {
                $result[] = $statement;
            }
            $script = substr($script, $matches[0][1] + 1);
        }
    }

    return $result;

}

function executeScriptFile($fileName, $dbConnection) {
    $script = file_get_contents($fileName);
    $statements = parseScript($script);
    foreach($statements as $statement) {
        mysqli_query($dbConnection, $statement);
    }
}

function createDatabase() {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $conn = new mysqli($servername, $username, $password);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "CREATE DATABASE php33";
    if (!$conn->query($sql)) {
        echo "Error creating database: " . $conn->error;
    }
    $conn->close();
}

$hostName = 'localhost';
$userName = 'root';
$password = '';
$dataBaseName = 'php33';
$port = '3306';
$fileName = 'php33.sql';

if ($connection = @mysqli_connect($hostName, $userName, $password, $dataBaseName, $port)) {
    executeScriptFile($fileName, $connection);
    header("Location: ../index.php");
    exit();
} else {
    createDatabase();
    echo "Created database ...";
    header("refresh: 0");
}
