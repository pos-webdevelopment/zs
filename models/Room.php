<?php
require_once "Database.php";
class Room
{
    private $rId = 0;
    private $rName = '';
    private $rPersonen = 0;
    private $rPreis = 0.0;
    private $rBalkon = false;
    private $errors = [];

    public static function getAll() {
        $db = Database::connect();
        $sql = 'SELECT * FROM tbl_rooms ORDER BY rId ASC, rName ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $room = $stmt->fetchAll(PDO::FETCH_CLASS, 'Room');
        Database::disconnect();
        return $room;
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM tbl_rooms WHERE rId = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Room');
        Database::disconnect();

        return $item !== false ? $item : null;
    }

    public function save()
    {
        if ($this->validate()) {
            if ($this->rId != null && $this->rId > 0) {
                // known ID > 0 -> old object -> update
                $this->update();
            } else {
                // undefined ID -> new object -> create
                $this->rId = $this->create();
            }
            return true;
        }
        return false;
    }

    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_rooms (rName, rPersonen, rPreis, rBalkon) values(?, ?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->rName, $this->rPersonen, $this->rPreis, $this->rBalkon));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    public static function getNextNumber() {
        $db = Database::connect();
        $data = array();
        $sql = "SELECT Max(rId) FROM tbl_rooms;";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $id = $stmt->fetchObject('Room');
        $data['id'] = $id->rId;
        Database::disconnect();
        return $data;
    }

    /**
     * Saves the object to the database
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE tbl_rooms set rName = ?, rPersonen = ?, rPreis = ?, rBalkon = ? WHERE rId = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->rName, $this->rPersonen, $this->rPreis, $this->rBalkon, $this->rId));
        Database::disconnect();
    }

    public function validate()
    {
        return $this->validateHelper('Name', 'name', $this->rName, 32) &&
            $this->validateNumber($this->rPreis, 'preis') && $this->validateNumber($this->rPersonen, 'balcony');
    }

    private function validateHelper($label, $key, $value, $maxLength)
    {
        if (strlen($value) == 0) {
            $this->errors[$key] = "$label darf nicht leer sein";
            return false;
        } else if (strlen($value) > $maxLength) {
            $this->errors[$key] = "$label zu lang (max. $maxLength Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    private function validateNumber($value, $label) {
        if ($value == 0) {
            $this->errors[$value] = $label . " darf nicht 0 sein";
            return false;
        } else {
            return true;
        }
    }

    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM tbl_rooms WHERE rId = ?";
        $stmt = $db->prepare($sql);
        try {
            $success = $stmt->execute(array($id));
            if (!$success) {
                return false;
            } else {
                return true;
            }
        } catch (PDOException $exception) {
        }
        Database::disconnect();
    }

    /**
     * @return int
     */
    public function getRId()
    {
        return $this->rId;
    }

    /**
     * @return string
     */
    public function getRName()
    {
        return $this->rName;
    }

    /**
     * @return int
     */
    public function getRPersonen()
    {
        return $this->rPersonen;
    }

    /**
     * @return float
     */
    public function getRPreis()
    {
        return $this->rPreis;
    }

    /**
     * @return bool
     */
    public function isRBalkon()
    {
        return $this->rBalkon;
    }

    /**
     * @param int $rId
     */
    public function setRId($rId)
    {
        $this->rId = $rId;
    }

    /**
     * @param string $rName
     */
    public function setRName($rName)
    {
        $this->rName = $rName;
    }

    /**
     * @param int $rPersonen
     */
    public function setRPersonen($rPersonen)
    {
        $this->rPersonen = $rPersonen;
    }

    /**
     * @param float $rPreis
     */
    public function setRPreis($rPreis)
    {
        $this->rPreis = $rPreis;
    }

    /**
     * @param bool $rBalkon
     */
    public function setRBalkon($rBalkon)
    {
        $this->rBalkon = $rBalkon;
    }

}