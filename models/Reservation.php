<?php

require_once "Database.php";
class Reservation
{
    private $vId = 0;
    private $vStart = '';
    private $vEnde = '';
    private $rId = 0;
    private $gId = 0;

    private $rName = '';
    private $gLastname = '';
    private $gFirstname = '';

    private $errors = [];

    public static function getAll() {
        $db = Database::connect();
        $sql = 'SELECT * from tbl_reservation v 
                INNER JOIN tbl_rooms r ON v.rId = r.rId
                INNER JOIN tbl_guests g ON v.gId = g.gId
                ORDER BY vStart ASC, vEnde ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $reservation = $stmt->fetchAll(PDO::FETCH_CLASS, 'Reservation');
        Database::disconnect();
        return $reservation;
    }
    public static function getActiveReservations() {
        $db = Database::connect();
        $sql = 'SELECT * from tbl_reservation v 
                INNER JOIN tbl_rooms r ON v.rId = r.rId
                INNER JOIN tbl_guests g ON v.gId = g.gId
                WHERE v.vStart < now()
                ORDER BY vStart ASC, vEnde ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $reservation = $stmt->fetchAll(PDO::FETCH_CLASS, 'Reservation');
        Database::disconnect();
        return $reservation;
    }

    public static function get($id) {
        $db = Database::connect();
        $sql = "SELECT * from tbl_reservation v 
                INNER JOIN tbl_rooms r ON v.rId = r.rId
                INNER JOIN tbl_guests g ON v.gId = g.gId WHERE vId = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $reservation = $stmt->fetchObject('Reservation');
        Database::disconnect();
        return $reservation !== false ? $reservation : null;
    }

    public function save()
    {
        if ($this->validate()) {
            if ($this->vId != null && $this->vId > 0) {
                $this->update();
            } else {
                if($this->verifyNewBooking() == null) {
                    $this->vId = $this->create();
                    return true;
                } else {
                    $this->errors['error'] = "Fehler aufgetreten.";
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_reservation(vStart, vEnde, rId, gId) VALUES (?, ?, ?, ?);";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->vStart, $this->vEnde, $this->rId, $this->gId));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE tbl_reservation set vStart = ?, vEnde = ?, rId = ?, gId = ? WHERE vId = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->vStart, $this->vEnde, $this->rId, $this->gId, $this->vId));
        Database::disconnect();
    }

    public function validate()
    {
        if($this->vStart<$this->vEnde) {
            return true;
        } else {
            $this->errors['date'] = "Datum stimmt nicht";
            return false;
        }
    }

    public function verifyNewBooking() {
        $db = Database::connect();
        // Parameter: vStart, vEnde, rId, vStart, rId
        $sql = "SELECT * FROM tbl_reservation WHERE vStart BETWEEN ? AND ? AND rId = ? OR ? BETWEEN vStart AND vEnde AND rId = ?;";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->vStart, $this->vEnde, $this->rId, $this->vStart, $this->rId));
        $reservation = $stmt->fetchObject('Reservation');
        Database::disconnect();
        return $reservation !== false ? $reservation : null;
    }

    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM tbl_reservation WHERE vId = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    /**
     * @return int
     */
    public function getVId()
    {
        return $this->vId;
    }

    /**
     * @param int $vId
     */
    public function setVId($vId)
    {
        $this->vId = $vId;
    }

    /**
     * @return string
     */
    public function getVStart()
    {
        return $this->vStart;
    }

    /**
     * @param string $vStart
     */
    public function setVStart($vStart)
    {
        $this->vStart = $vStart;
    }

    /**
     * @return string
     */
    public function getVEnde()
    {
        return $this->vEnde;
    }

    /**
     * @param string $vEnde
     */
    public function setVEnde($vEnde)
    {
        $this->vEnde = $vEnde;
    }

    /**
     * @return int
     */
    public function getRId()
    {
        return $this->rId;
    }

    /**
     * @param int $rId
     */
    public function setRId($rId)
    {
        $this->rId = $rId;
    }

    /**
     * @return int
     */
    public function getGId()
    {
        return $this->gId;
    }

    /**
     * @param int $gId
     */
    public function setGId($gId)
    {
        $this->gId = $gId;
    }

    /**
     * @return string
     */
    public function getRName()
    {
        return $this->rName;
    }

    /**
     * @param string $rName
     */
    public function setRName($rName)
    {
        $this->rName = $rName;
    }

    /**
     * @return string
     */
    public function getGLastname()
    {
        return $this->gLastname;
    }

    /**
     * @param string $gLastname
     */
    public function setGLastname($gLastname)
    {
        $this->gLastname = $gLastname;
    }

    /**
     * @return string
     */
    public function getGFirstname()
    {
        return $this->gFirstname;
    }

    /**
     * @param string $gFirstname
     */
    public function setGFirstname($gFirstname)
    {
        $this->gFirstname = $gFirstname;
    }

    /**
     * @return array
     */
    public function getErrors($key)
    {
        return $this->errors[$key];
    }


}