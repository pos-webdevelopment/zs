DROP DATABASE IF EXISTS php33;
create database if not exists php33;
use  php33;
DROP TABLE IF EXISTS tbl_rooms;
CREATE TABLE IF NOT EXISTS tbl_rooms (
rId INTEGER  NOT NULL PRIMARY KEY AUTO_INCREMENT,
rName VARCHAR(255) NOT NULL,
rPersonen INTEGER  NOT NULL,
rPreis DOUBLE  NOT NULL,
rBalkon BOOLEAN  NOT NULL
);

DROP TABLE IF EXISTS tbl_guests;
CREATE TABLE IF NOT EXISTS tbl_guests (
gId INTEGER NOT NULL auto_increment,
gFirstname varchar(255) NOT NULL,
gLastname varchar(255) NOT NULL,
gEmail varchar(255) NOT NULL,
primary key(gId),
unique(gFirstname, gLastname, gEmail)
);

DROP TABLE IF EXISTS tbl_reservation;
CREATE TABLE IF NOT EXISTS tbl_reservation(
vId INTEGER NOT NULL AUTO_INCREMENT,
vStart DATE NOT NULL,
vEnde DATE NOT NULL,
rId INTEGER NOT NULL,
gId INTEGER NOT NULL,
PRIMARY KEY(vId),
FOREIGN KEY(rId) REFERENCES tbl_rooms(rId), 
FOREIGN KEY(gId) REFERENCES tbl_guests(gId)
);

INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Tres-Zap',4,53.72,0);
INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Pannier',2,53.72,0);
INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Gembucket',1,81.87,1);
INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Konklab',4,99.81,0);
INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Zontrax',1,77.96,1);
INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Veribet',1,85.50,0);
INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Zoolab',1,36.51,0);
INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Keylex',3,64.09,0);
INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Regrant',2,43.35,0);
INSERT INTO tbl_rooms(rName,rPersonen,rPreis,rBalkon) VALUES ('Namfix',3,93.03,1);

INSERT INTO tbl_guests(gFirstname, gLastname, gEmail) VALUES ('Max', 'Mustermann', 'max.mustermann@mail.com');
INSERT INTO tbl_guests(gFirstname, gLastname, gEmail) VALUES ('Andrea', 'Froehlich', 'afroehlich@mail.at');
INSERT INTO tbl_guests(gFirstname, gLastname, gEmail) VALUES ('James', 'Byott', 'byott@mail.co.uk');
INSERT INTO tbl_guests(gFirstname, gLastname, gEmail) VALUES ('Susi', 'Meier', 'meier-clan@mail.de');
INSERT INTO tbl_guests(gFirstname, gLastname, gEmail) VALUES ('Fritz', 'Gruber', 'gruber@beton.at');

INSERT INTO tbl_reservation(vStart, vEnde, rId, gId) VALUES ('2021-01-21', '2021-01-30', 8, 1);
INSERT INTO tbl_reservation(vStart, vEnde, rId, gId) VALUES ('2021-02-02', '2021-02-10', 5, 3);
## INSERT INTO tbl_reservation(vStart, vEnde, rId, gId) VALUES ('2021-01-22', '2021-01-25', 2, 2);