<?php

require_once "Database.php";

class Guests
{
    private $gId = 0;
    private $gFirstname = '';
    private $gLastname = '';
    private $gEmail = '';
    private $errors = [];

    public static function getAll()
    {
        $db = Database::connect();
        $sql = 'SELECT * FROM tbl_guests ORDER BY gId ASC, gLastname ASC';
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $guest = $stmt->fetchAll(PDO::FETCH_CLASS, 'Guests');
        Database::disconnect();
        return $guest;
    }

    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM tbl_guests WHERE gId = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $item = $stmt->fetchObject('Guests');
        Database::disconnect();
        return $item !== false ? $item : null;
    }

    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM tbl_guests WHERE gId = ?";
        $stmt = $db->prepare($sql);
        try {
            $success = $stmt->execute(array($id));
            if (!$success) {
                return false;
            } else {
                return true;
            }
        } catch (PDOException $exception) {
        }
        Database::disconnect();
    }

    public function save()
    {
        if ($this->validate()) {
            if ($this->gId != null && $this->gId > 0) {
                // known ID > 0 -> old object -> update
                $this->update();
            } else {
                // undefined ID -> new object -> create
                $this->gId = $this->create();
            }
            return true;
        }
        return false;
    }

    public function validate()
    {
        return $this->validateHelper('Vorname', 'first', $this->gFirstname, 64) &&
            $this->validateHelper('Nachname', 'last', $this->gLastname, 64) &&
            $this->validateHelper('E-Mail', 'mail', $this->gEmail, 32);
    }

    private function validateHelper($label, $key, $value, $maxLength)
    {
        if (strlen($value) == 0) {
            $this->errors[$key] = "$label darf nicht leer sein";
            return false;
        } else if (strlen($value) > $maxLength) {
            $this->errors[$key] = "$label zu lang (max. $maxLength Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE tbl_guests set gFirstname = ?, gLastname = ?, gEmail = ? WHERE gId = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->gFirstname, $this->gLastname, $this->gEmail, $this->gId));
        Database::disconnect();
    }

    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO tbl_guests(gFirstname, gLastname, gEmail) VALUES (?, ?, ?);";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->gFirstname, $this->gLastname, $this->gEmail));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    /**
     * @return int
     */
    public function getGId()
    {
        return $this->gId;
    }

    /**
     * @param int $gId
     */
    public function setGId($gId)
    {
        $this->gId = $gId;
    }

    /**
     * @return string
     */
    public function getGFirstname()
    {
        return $this->gFirstname;
    }

    /**
     * @param string $gFirstname
     */
    public function setGFirstname($gFirstname)
    {
        $this->gFirstname = $gFirstname;
    }

    /**
     * @return string
     */
    public function getGLastname()
    {
        return $this->gLastname;
    }

    /**
     * @param string $gLastname
     */
    public function setGLastname($gLastname)
    {
        $this->gLastname = $gLastname;
    }

    /**
     * @return string
     */

    /**
     * @return string
     */
    public function getGEmail()
    {
        return $this->gEmail;
    }

    /**
     * @param string $gEmail
     */
    public function setGEmail($gEmail)
    {
        $this->gEmail = $gEmail;
    }


}