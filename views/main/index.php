<?php
$title = "Verwaltung";
include '../layouts/top.php';
require_once "../../models/Reservation.php";
try {
    $reservation = Reservation::getActiveReservations();
} catch (PDOException $exception) {
    header("Location: ../views/main/install.php");
    exit();
}
Database::disconnect();
?>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2><?= $title ?> - zurzeit aktive Buchungen</h2>
        </div>
        <div class="col-md-6">
            <img src="../../img/keys.png" alt="keys" width="75">
        </div>

    </div>
    <div class="row">
        <table class="table table-striped table-bordered">
            <tbody>
            <thead>
            <tr>
                <th>gebuchtes Zimmer</th>
                <th>Gast</th>
                <th>Aufenthaltsdauer gesamt</th>
                <th>Aufenthaltsdauer verbleibend</th>
                <th>Abreise-Datum</th>
            </tr>
            </thead>
            <tr>
                <?php
                foreach ($reservation as $v) {
                echo '<tr><td><a href="../room/view.php?id='. $v->getRId() . '">[' . $v->getRId() . '] ' . $v->getRName() . '</a></td>';
                echo '<td><a href="../guest/view.php?id='.$v->getGId().'">' . $v->getGFirstname() . ' ' . $v->getGLastname() . '</a></td>';
                try {
                    $newStart = new DateTime($v->getVStart());
                    $newEnde = new DateTime($v->getVEnde());
                    $today = new DateTime('now');
                } catch (Exception $e) {
                    echo "Dauer konnte nicht berechnet werden.";
                }
                $duration = date_diff($newStart, $newEnde);
                $remaining = date_diff($today, $newEnde);
                echo '<td>' . $duration->format('%a Tage') . '</td>';
                echo '<td>' . $remaining->format('%a Tage') . '</td>';
                echo '<td>' . date("l, d. M Y", strtotime($v->getVEnde())) . '</td>';
                ?>
            </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div> <!-- /container -->
<br><a href="install.php" class="center-block text-center">Datenbank neu installieren</a>
<?php
include '../layouts/bottom.php';
?>

