<?php
$title = "Datenbank installieren";
include '../layouts/top.php';
?>
<div class="container">
    <h1>Datenbank installieren</h1>
    <br>
    <div class="row">
        <a href="../../models/reset.php" class="btn btn-primary">Installation starten</a>
        <a href="../room/index.php" class="btn btn-default">Zurück</a>
    </div>

</div>
<br><br><br>

<?php
include '../layouts/bottom.php';
?>
