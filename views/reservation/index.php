<?php
$title = "Reservierungen";
include '../layouts/top.php';
require_once "../../models/Reservation.php";
try {
    Reservation::getAll();
} catch (PDOException $exception) {
    header("Location: ../main/install.php");
    exit();

}
Database::disconnect();
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <tbody>
                <thead>
                <tr>
                    <th>Reservierungs-ID</th>
                    <th>Start</th>
                    <th>Ende</th>
                    <th>Raum-Name</th>
                    <th>Gast</th>
                    <th>Verwaltung</th>
                </tr>
                </thead>
                <tr>
                <?php
                $reservation = Reservation::getAll();
                foreach ($reservation as $r) {
                    echo '<tr><td>'. $r->getVId() . '</td>';
                    echo '<td>'. date("l, d. M Y", strtotime($r->getVStart())) . '</td>';
                    echo '<td>'. date("l, d. M Y", strtotime($r->getVEnde())) . '</td>';
                    echo '<td>'. $r->getRName() . '</td>';
                    echo '<td>'. $r->getGFirstname(). ' ' . $r->getGLastname() . '</td>';
                    ?>
                    <td><a class="btn btn-info" href="view.php?id=<?= $r->getVId() ?>"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                                class="btn btn-primary" href="update.php?id=<?= $r->getVId() ?>"><span
                                    class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                                class="btn btn-danger" href="delete.php?id=<?= $r->getVId() ?>"><span
                                    class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>