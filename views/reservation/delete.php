<?php
$title = "Zimmer löschen";
include '../layouts/top.php';
require_once "../../models/Reservation.php";
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    // load single item per ID
    $v = Reservation::get($_GET['id']);
}

// check if item could be found
if ($v == null) {
    http_response_code(404);    // item not found
    die();
}
if(isset($_POST['submit'])) {
    if(!empty($_POST['id'])) {
        Reservation::delete($_POST['id']);
        header("Location: index.php");
        exit();
    }
}

?>

    <div class="container">
        <h2><?= $title ?></h2>

        <form class="form-horizontal" action="delete.php?id=<?= $v->getVId() ?>" method="post">
            <input type="hidden" name="id" value="<?= $v->getVId() ?>"/>
            <p class="alert alert-error">Möchten Sie die Reservierung Nr. <?= $v->getVId() ?> von <?= $v->getGLastname() . ', ' . $v->getGFirstname() ?> wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" name="submit" class="btn btn-danger">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>