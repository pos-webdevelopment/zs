<?php
$title = "Reservierung erstellen";
include '../layouts/top.php';
require_once "../../models/Reservation.php";
$v = new Reservation();
if(isset($_POST['start']) && isset($_POST['ende']) && isset($_POST['rid']) && isset($_POST['gid'])) {
    if($_POST['start']<$_POST['ende']) {
        if (!empty($_POST)) {
            $v->setVStart(isset($_POST['start']) ? $_POST['start'] : '');
            $v->setVEnde(isset($_POST['ende']) ? $_POST['ende'] : '');
            $v->setRId(isset($_POST['rid']) ? $_POST['rid'] : '');
            $v->setGId(isset($_POST['gid']) ? $_POST['gid'] : '');
            if ($v->save()) {
                header("Location: view.php?id=" . $v->getVId());
                exit();
            }
        }
    }
}
?>
    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>

        <form class="form-horizontal" action="create.php" method="post">

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Start-Datum *</label>
                        <input type="date" class="form-control <?php if($v->getErrors('error')) { echo 'btn-danger'; } ?>" name="start" value="0">
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">End-Datum *</label>
                        <input type="date" class="form-control <?php if($v->getErrors('error')) { echo 'btn-danger'; } ?>" name="ende" value="0">
                    </div>
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Zimmer *</label>
                        <select name="rid" class="form-control" required>
                            <option disabled selected value="0">--- Bitte wählen ---</option>
                            <?php
                            require_once "../../models/Room.php";
                            $room = Room::getAll();
                            $current = $v->getRId();
                            foreach ($room as $r) {
                                ?>
                                <option value='<?= $r->getRId()?>' <?php if($r->getRId() == $current) echo "selected='selected'" ?>> <?= $r->getRId() . ': ' . $r->getRName() ?> </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Gast *</label>
                        <select name="gid" class="form-control" required>
                            <option disabled selected value="0">--- Bitte wählen ---</option>
                            <?php
                            require_once "../../models/Guests.php";
                            $guest = Guests::getAll();
                            $current = $v->getGId();
                            foreach ($guest as $g) {
                                ?>
                                <option value='<?= $g->getGId()?>' <?php if($g->getGId() == $current) echo "selected='selected'" ?>> <?= $g->getGFirstname(). ' ' . $g->getGLastname() ?> </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>