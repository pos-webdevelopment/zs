<?php
$title = "Reservierung bearbeiten";
include '../layouts/top.php';
require_once '../../models/Reservation.php';

// load single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    // load single item per ID
    $v = Reservation::get($_GET['id']);
}

// check if item could be found
if ($v == null) {
    http_response_code(404);    // item not found
    die();
}

if (!empty($_POST)) {
    $v->setVStart(isset($_POST['start']) ? $_POST['start'] : '');
    $v->setVEnde(isset($_POST['ende']) ? $_POST['ende'] : '');
    $v->setRId(isset($_POST['rid']) ? $_POST['rid'] : '');
    $v->setGId(isset($_POST['gid']) ? $_POST['gid'] : '');

    // update existing item and redirec to index-page
    if ($v->save()) {
        header("Location: view.php?id=" . $v->getVId());
        exit();
    }
}
?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $v->getVId() ?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <?php
               if (isset($v->getErrors['date'])) {
                   echo $v->getErrors['date'];
               }
                ?>
                <div class="form-group required ">
                    <label class="control-label">Start-Datum *</label>
                    <input type="date" class="form-control" name="start" value="<?= $v->getVStart() ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">End-Datum *</label>
                    <input type="date" class="form-control" name="ende" value="<?= $v->getVEnde() ?>">
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Zimmer *</label>
                    <select name="rid" class="form-control" required>
                        <?php
                        require_once "../../models/Room.php";
                        $room = Room::getAll();
                        $current = $v->getRId();
                        foreach ($room as $r) {
                            ?>
                            <option value='<?= $r->getRId()?>' <?php if($r->getRId() == $current) echo "selected='selected'" ?>> <?= $r->getRId() . ': ' . $r->getRName() ?> </option>
                        <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Gast *</label>
                    <select name="gid" class="form-control" required>
                        <?php
                        require_once "../../models/Guests.php";
                        $guest = Guests::getAll();
                        $current = $v->getGId();
                        foreach ($guest as $g) {
                            ?>
                            <option value='<?= $g->getGId()?>' <?php if($g->getGId() == $current) echo "selected='selected'" ?>> <?= $g->getGFirstname(). ' ' . $g->getGLastname() ?> </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
