<?php
$title = "Reservierung anzeigen";
include '../layouts/top.php';
require_once "../../models/Reservation.php";
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    // load single item per ID
    $r = Reservation::get($_GET['id']);
}

// check if item could be found
if ($r == null) {
    http_response_code(404);    // item not found
    die();
}
?>
    <script>
        function goback() {
            history.go(-1);
        }
    </script>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $r->getVId() ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $r->getVId() ?>">Löschen</a>
            <a class="btn btn-default" href="javascript:goback()">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>Reservierungs-Nummer</th>
                <td><?= $r->getVId() ?></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>Start-Datum</th>
                <td><?= date("l, d. M Y", strtotime($r->getVStart())) ?></td>
            </tr>
            <tr>
                <th>End-Datum</th>
                <td><?= date("l, d. M Y", strtotime($r->getVEnde())) ?></td>
            </tr>
            <tr>
                <th>Aufenthaltsdauer</th>
                <td><?php
                    try {
                        $newStart = new DateTime($r->getVStart());
                        $newEnde = new DateTime($r->getVEnde());
                    } catch (Exception $e) {
                        echo "Dauer konnte nicht berechnet werden.";
                    }
                    $duration = date_diff($newStart, $newEnde);
                    echo $duration->format('%a Tage');
                    ?></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>Zimmer-Nummer</th>
                <td><?= $r->getRId() ?></td>
            </tr>
            <tr>
                <th>Zimmer-Name</th>
                <td><?= $r->getRName() ?></td>
            </tr>
            <tr>
                <th>Details</th>
                <td><a href="../room/view.php?id=<?= $r->getRId() ?>">DETAILS zu <?= $r->getRName()?></a></td>
            </tr>
            <tr>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <th>Gast-ID</th>
                <td><?= $r->getGId() ?></td>
            </tr>
            <tr>
                <th>Gast Vorname</th>
                <td><?= $r->getGFirstname() ?></td>
            </tr>
            <tr>
                <th>Gast Nachname</th>
                <td><?= $r->getGLastname() ?></td>
            </tr>
            <tr>
                <th>Details</th>
                <td><a href="../guest/view.php?id=<?= $r->getGId() ?>">DETAILS zu <?= $r->getGLastname()?></a></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>