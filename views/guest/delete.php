<?php
$title = "Gast löschen";
$message = null;
include '../layouts/top.php';
require_once "../../models/Guests.php";
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    // load single item per ID
    $g = Guests::get($_GET['id']);
}

// check if item could be found
if ($g == null) {
    http_response_code(404);    // item not found
    die();
}
if(isset($_POST['submit'])) {
    if(!empty($_POST['id'])) {
        if(Guests::delete($_POST['id'])) {
            header("Location: index.php");
            exit();
        } else {
            $message = "Löschen nicht möglich! Dieser Gast hat laufende Buchungen!";
        }
    }
}
?>

    <div class="container">
        <h2><?= $title ?></h2>
        <?php
        if($message != null) {
            ?>
        <div class="alert alert-danger">
            <?php
                echo $message;
            ?>
        </div>
            <?php } ?>
        <form class="form-horizontal" action="delete.php?id=<?= $g->getGId() ?>" method="post">
            <input type="hidden" name="id" value="<?= $g->getGId() ?>"/>
            <p class="alert alert-error">Wollen Sie den Gast <?= $g->getGLastname() . ', ' . $g->getGFirstname() ?> wirklich löschen?</p>
            <div class="form-actions">
                <button type="submit" name="submit" class="btn btn-danger">Löschen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>