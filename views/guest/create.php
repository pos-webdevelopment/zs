<?php
$title = "Gast anlegen";
include '../layouts/top.php';
require_once "../../models/Guests.php";
$g = new Guests();
if (!empty($_POST)) {
    $g->setGFirstname(isset($_POST['first']) ? $_POST['first'] : '');
    $g->setGLastname(isset($_POST['last']) ?$_POST['last'] : '');
    $g->setGEmail(isset($_POST['mail']) ? $_POST['mail'] : '');

    // redirect to view-page after successful saving
    if ($g->save()) {
        header("Location: view.php?id=" . $g->getGId());
        exit();
    }
}
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>

        <form class="form-horizontal" action="create.php" method="post">

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">Vorname *</label>
                        <input type="text" class="form-control" name="first" maxlength="64" value="<?= $g->getGFirstname() ?>">
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-4">
                    <div class="form-group required ">
                        <label class="control-label">Nachname *</label>
                        <input type="text" class="form-control" name="last" maxlength="64" value="<?= $g->getGLastname() ?>">
                    </div>
                </div>
                <div class="col-md-5"></div>
            </div>

            <div class="row">
                <div class="col-md-2">
                    <div class="form-group required ">
                        <label class="control-label">E-Mail *</label>
                        <input type="email" class="form-control" name="mail" value="<?= $g->getGEmail() ?>">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
        </form>

    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>