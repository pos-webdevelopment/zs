<?php
$title = "Gästeverwaltung";
include '../layouts/top.php';
require_once "../../models/Guests.php";
try {
    Guests::getAll();
} catch (PDOException $exception) {
    header("Location: ../main/install.php");
    exit();

}
Database::disconnect();
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <tbody>
                <thead>
                <tr>
                    <th>Gast-ID</th>
                    <th>Name</th>
                    <th>E-Mail</th>
                    <th>Verwaltung</th>
                </tr>
                </thead>
                <tr>
                <?php
                require_once "../../models/Guests.php";
                $guests = Guests::getAll();
                foreach ($guests as $g) {
                    echo '<tr><td>'. $g->getGId() . '</td>';
                    echo '<td>'. $g->getGFirstname() . ' ' . $g->getGLastname() . '</td>';
                    echo '<td>'. $g->getGEMail() . '</td>';
                    ?>
                    <td><a class="btn btn-info" href="view.php?id=<?= $g->getGId() ?>"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                                class="btn btn-primary" href="update.php?id=<?= $g->getGId() ?>"><span
                                    class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                                class="btn btn-danger" href="delete.php?id=<?= $g->getGId() ?>"><span
                                    class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>