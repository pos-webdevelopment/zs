<?php
$title = "Kontaktdaten bearbeiten";
include '../layouts/top.php';
require_once '../../models/Guests.php';

// load single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    // load single item per ID
    $g = Guests::get($_GET['id']);
}

// check if item could be found
if ($g == null) {
    http_response_code(404);    // item not found
    die();
}

if (!empty($_POST)) {
    $g->setGFirstname(isset($_POST['first']) ? $_POST['first'] : '');
    $g->setGLastname(isset($_POST['last']) ?$_POST['last'] : '');
    $g->setGEmail(isset($_POST['mail']) ? $_POST['mail'] : '');

    // update existing item and redirec to index-page
    if ($g->save()) {
        header("Location: view.php?id=" . $g->getGId());
        exit();
    }
}
?>
<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $g->getGId() ?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Vorname *</label>
                    <input type="text" class="form-control" name="first" maxlength="64" value="<?= $g->getGFirstname() ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Nachname *</label>
                    <input type="text" class="form-control" name="last" maxlength="64" value="<?= $g->getGLastname() ?>">
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">E-Mail *</label>
                    <input type="email" class="form-control" name="mail" value="<?= $g->getGEmail() ?>">
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
