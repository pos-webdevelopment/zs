<?php
$title = "Gast anzeigen";
include '../layouts/top.php';
require_once "../../models/Guests.php";
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    // load single item per ID
    $g = Guests::get($_GET['id']);
}

// check if item could be found
if ($g == null) {
    http_response_code(404);    // item not found
    die();
}
?>
    <script>
        function goback() {
            history.go(-1);
        }
    </script>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $g->getGId() ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $g->getGId() ?>">Löschen</a>
            <a class="btn btn-default" href="javascript:goback()">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>Gast-ID</th>
                <td><?= $g->getGId() ?></td>
            </tr>
            <tr>
                <th>Vorname</th>
                <td><?= $g->getGFirstname() ?></td>
            </tr>
            <tr>
                <th>Nachname</th>
                <td><?= $g->getGLastname() ?></td>
            </tr>
            <tr>
                <th>E-Mail-Adresse</th>
                <td><?= $g->getGEmail() ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>