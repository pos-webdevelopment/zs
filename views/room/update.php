<?php
$title = "Zimmer bearbeiten";
include '../layouts/top.php';
require_once '../../models/Room.php';

// load single item (per ID), redirect to index if no ID is present (HTTP GET-parameter)
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    // load single item per ID
    $r = Room::get($_GET['id']);
}

// check if item could be found
if ($r == null) {
    http_response_code(404);    // item not found
    die();
}

if (!empty($_POST)) {
    $r->setRName(isset($_POST['name']) ? $_POST['name'] : '');
    $r->setRPersonen(isset($_POST['size']) ?$_POST['size'] : '');
    $r->setRBalkon(isset($_POST['balcony']) ? 1 : 0);
    $r->setRPreis(isset($_POST['price']) ? $_POST['price'] : '');

    // update existing item and redirec to index-page
    if ($r->save()) {
        header("Location: view.php?id=" . $r->getRId());
        exit();
    }
}
?>
?>

<div class="container">
    <div class="row">
        <h2><?= $title ?></h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $r->getRId() ?>" method="post">

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Zimmernummer *</label>
                    <input type="text" class="form-control" name="nr" maxlength="4" value="<?= $r->getRId() ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4">
                <div class="form-group required ">
                    <label class="control-label">Name *</label>
                    <input type="text" class="form-control" name="name" maxlength="64" value="<?= $r->getRName() ?>">
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Personen *</label>
                    <input type="number" class="form-control" name="size" min="1" value="<?= $r->getRPersonen() ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required ">
                    <label class="control-label">Preis *</label>
                    <input type="text" class="form-control" name="price" value="<?= $r->getRPreis() ?>">
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-1">
                <div class="form-group required ">
                    <label class="control-label">Balkon *</label>
                    <input type="checkbox" class="form-control" name="balcony" <?php if($r->isRBalkon()) {echo 'checked="checked"';} else {echo '';}  ?>>
                </div>
            </div>
            <div class="col-md-5"></div>
        </div>

        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>

</div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>
