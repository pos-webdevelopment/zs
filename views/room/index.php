<?php
$title = "Zimmerverwaltung";
include '../layouts/top.php';
require_once "../../models/Room.php";
try {
    Room::getAll();
} catch (PDOException $exception) {
    header("Location: ../main/install.php");
    exit();

}
Database::disconnect();
?>

    <div class="container">
        <div class="row">
            <h2><?= $title ?></h2>
        </div>
        <div class="row">
            <p>
                <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
            </p>

            <table class="table table-striped table-bordered">
                <tbody>
                <thead>
                <tr>
                    <th>Zimmernummer</th>
                    <th>Name</th>
                    <th>Personen</th>
                    <th>Preis</th>
                    <th>Balkon</th>
                    <th>Verwaltung</th>
                </tr>
                </thead>
                <tr>
                <?php
                require_once "../../models/Room.php";
                $rooms = Room::getAll();
                foreach ($rooms as $r) {
                    echo '<tr><td>'. $r->getRId() . '</td>';
                    echo '<td>'. $r->getRName() . '</td>';
                    echo '<td>'. $r->getRPersonen() . '</td>';
                    echo '<td>'. $r->getRPreis() . '</td>';
                    if($r->isRBalkon()) {
                        $b = "JA";
                    } else {
                        $b = "NEIN";
                    }
                    echo '<td>'. $b  . '</td>';
                    ?>
                    <td><a class="btn btn-info" href="view.php?id=<?= $r->getRId() ?>"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                                class="btn btn-primary" href="update.php?id=<?= $r->getRId() ?>"><span
                                    class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                                class="btn btn-danger" href="delete.php?id=<?= $r->getRId() ?>"><span
                                    class="glyphicon glyphicon-remove"></span></a>
                    </td>
                </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>