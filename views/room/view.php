<?php
$title = "Zimmer anzeigen";
include '../layouts/top.php';
require_once "../../models/Room.php";
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    // load single item per ID
    $r = Room::get($_GET['id']);
}

// check if item could be found
if ($r == null) {
    http_response_code(404);    // item not found
    die();
}
?>
    <script>
        function goback() {
            history.go(-1);
        }
    </script>

    <div class="container">
        <h2><?= $title ?></h2>

        <p>
            <a class="btn btn-primary" href="update.php?id=<?= $r->getRId() ?>">Aktualisieren</a>
            <a class="btn btn-danger" href="delete.php?id=<?= $r->getRId() ?>">Löschen</a>
            <a class="btn btn-default" href="javascript:goback()">Zurück</a>
        </p>

        <table class="table table-striped table-bordered detail-view">
            <tbody>
            <tr>
                <th>Zimmernummer</th>
                <td><?= $r->getRId() ?></td>
            </tr>
            <tr>
                <th>Name</th>
                <td><?= $r->getRName() ?></td>
            </tr>
            <tr>
                <th>Personen</th>
                <td><?= $r->getRPersonen() ?></td>
            </tr>
            <tr>
                <th>Preis</th>
                <td>EUR <?= $r->getRPreis() ?></td>
            </tr>
            <tr>
                <th>Balkon</th>
                <td><?php if($r->isRBalkon()==0) {echo "Nein";} else {echo "Ja";} ?></td>
            </tr>
            </tbody>
        </table>
    </div> <!-- /container -->

<?php
include '../layouts/bottom.php';
?>